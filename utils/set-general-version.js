const path = require('path');

const { readPackage, findPackages } = require('../src');

const versionRegEx = /^\d+\.\d+\.\d+$/;

const [version, packagesPath = '.', force] = (() => {
  const argv = process.argv.slice(2);
  const v = argv.indexOf('-v');
  const p = argv.indexOf('-p');
  const f = argv.indexOf('-f');
  return [...[v, p].map((i) => (i < 0 ? undefined : argv[i + 1])), f >= 0];
})();

if (!versionRegEx.test(version)) {
  console.error(
    `\x1b[31mFailed:\x1b[0m -v param must be \x1b[32m${versionRegEx.source}\x1b[0m`
  );
  process.exit(1);
  return;
}

const packages = findPackages(path.resolve(process.cwd(), packagesPath));
const monorepo = readPackage(path.resolve(process.cwd(), packagesPath, '..'));

const major = monorepo.version.split('.')[0];

function processPkg({ pkg, save }) {
  const needChange = pkg.version !== version;

  const [namesp, name] = pkg.name.split(`/`);

  console.log(
    `Package ${
      namesp ? `\x1b[33m${namesp}\x1b[0m/` : ''
    }\x1b[32m${name}\x1b[0m ${
      needChange
        ? `change version \x1b[31m${pkg.version}\x1b[0m -> \x1b[32m${version}\x1b[0m`
        : `\x1b[90malready has version \x1b[32m${pkg.version}\x1b[0m`
    } `
  );

  if (!needChange) return;

  pkg.version = version;
  save({ silent: true });
}

Object.values(packages).forEach(
  (p) => (force || p.version.split('.')[0] === major) && processPkg(p)
);
processPkg(monorepo);

console.log('\x1b[32mSuccess!\x1b[0m');
