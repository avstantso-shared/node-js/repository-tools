const path = require('path');
const { findPackages, changeRepoDeps } = require('../src');

const allowDevTypes = ['workspace', 'url', 'version'];

const [depType, packagesPath = '.', packagesBase64] = (() => {
  const argv = process.argv.slice(2);
  const t = argv.indexOf('-t');
  const p = argv.indexOf('-p');
  const pkgs = argv.indexOf('-pkgs');
  return [t, p, pkgs].map((i) => (i < 0 ? undefined : argv[i + 1]));
})();

if (!allowDevTypes.includes(depType)) {
  console.error(
    `\x1b[31mFailed:\x1b[0m -t param must be one of %O`,
    allowDevTypes
  );
  process.exit(1);
  return;
}

const packages = packagesBase64
  ? JSON.parse(Buffer.from(packagesBase64, 'base64').toString('utf8'))
  : findPackages.public(path.resolve(process.cwd(), packagesPath));

changeRepoDeps(packages, depType);

console.log('\x1b[32mSuccess!\x1b[0m');
