const path = require('path');
const { execSync } = require('child_process');

const commands = ['checkout', 'pull', 'add', 'commit', 'push'];

const [command, rootPath = '.', message, branch = 'main'] = (() => {
  const argv = process.argv.slice(2);
  const c = argv.indexOf('-c');
  const p = argv.indexOf('-p');
  const m = argv.indexOf('-m');
  const b = argv.indexOf('-b');
  return [...[c, p, m, b].map((i) => (i < 0 ? undefined : argv[i + 1]))];
})();

if (!commands.includes(command)) {
  console.error(
    `\x1b[31mFailed:\x1b[0m -c param must be one of %O`,
    allowDevTypes
  );
  process.exit(1);
  return;
}

const cwd = path.resolve(process.cwd(), rootPath);

const cmdTails = {
  checkout: `git checkout ${branch}`,
  pull: `git pull`,
  add: `git add .`,
  commit: {
    win32: `'${path.resolve(
      __dirname,
      '..',
      'scripts',
      'commit.cmd'
    )}' ${message}`,
    linux: `sh -e -c 'git diff --quiet && git diff --staged --quiet || git commit -m "${message}"'`,
  },
  push: `git push`,
};

function getCmdTail() {
  let r = cmdTails[command];

  if ('object' === typeof r) r = r[process.platform];

  return r;
}

const result = execSync(`git submodule foreach ${getCmdTail()}`, {
  shell: true,
  cwd,
});

console.log(`${result}`);

console.log('\x1b[32mSuccess!\x1b[0m');
