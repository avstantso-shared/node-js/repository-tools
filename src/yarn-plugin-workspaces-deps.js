const findPackages = require('./find-packages');

function YarnPluginWorkspacesDeps(packagesPath) {
  return {
    name: `plugin-workspaces-deps`,
    factory: (require) => {
      let packages;

      const findPackage = ({ scope, name }) => packages[`@${scope}/${name}`];

      function reduceDependency(
        dependency,
        project,
        locator,
        initialDependency,
        { resolver, resolveOptions }
      ) {
        if (!packages) packages = findPackages.public(packagesPath);

        const { pkg } = findPackage(dependency) || {};

        if (pkg && 'workspace:*' !== dependency.range) {
          dependency.range = 'workspace:*';
        }

        return dependency;
      }

      function afterAllInstalled(project, options) {
        [...project.storedPackages.values()].forEach((spkg) => {
          const { pkg } = findPackage(spkg) || {};
          if (!pkg) return;

          ['dependencies', 'devDependencies'].forEach((depKey) => {
            const deps = spkg[depKey];
            if (!deps) return;

            [...deps.values()].forEach((dep) => {
              const { pkg: depPkg } = findPackage(dep) || {};
              if (!depPkg) return;

              dep.range = `${depPkg.repository.type}+${depPkg.repository.url}`;
            });
          });
        });

        return project.persist();
      }

      return { hooks: { reduceDependency, afterAllInstalled } };
    },
  };
}

module.exports = YarnPluginWorkspacesDeps;
