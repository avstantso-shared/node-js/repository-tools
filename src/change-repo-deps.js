function changeRepoDeps(packages, depType, { silent } = {}) {
  Object.values(packages).forEach(({ pkg, save }) => {
    let changed = false;

    ['dependencies', 'devDependencies'].forEach((depKey) => {
      const dep = pkg[depKey];

      if (!dep) return;

      Object.entries(dep).forEach(([name, oldValue]) => {
        const { pkg: refPkg } = packages[name] || {};

        if (!refPkg) return;

        const newValue = {
          workspace: () => 'workspace:*',
          url: (r = refPkg.repository) => `${r.type}+${r.url}`,
          version: () => refPkg.version,
        }[depType]();

        if (newValue === oldValue) return;

        dep[name] = newValue;
        changed = true;
      });
    });

    if (changed) save({ silent });
  });
}

module.exports = changeRepoDeps;
