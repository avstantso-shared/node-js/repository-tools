const https = require('https');

const httpsGet = async (url) =>
  new Promise((resolve, reject) =>
    https
      .get(url, (res) => {
        const data = [];
        res.on('data', (chunk) => data.push(chunk));
        res.on('end', () => resolve(Buffer.concat(data).toString()));
      })
      .on('error', reject)
  );

const gitlabFile = async (repoUrl, file, branch = 'main') =>
  httpsGet(`${repoUrl}/-/raw/${branch}/${file}?ref_type=heads&inline=false`);

module.exports = { httpsGet, gitlabFile };
