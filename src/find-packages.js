const path = require('path');
const fs = require('fs');

const readPackage = require('./read-package');

function findPackages(packagesPath, filter) {
  const packages = {};

  function walk(currentPath) {
    const p = readPackage(currentPath, filter);
    if (p) packages[p.name] = p;

    const dirs = fs.readdirSync(currentPath);
    dirs
      .map((dir) => path.resolve(currentPath, dir))
      .filter((dir) => fs.lstatSync(dir).isDirectory())
      .forEach(walk);
  }

  walk(packagesPath);

  return packages;
}

findPackages.public = (packagesPath = '.') =>
  findPackages(
    packagesPath,
    (pkg) => pkg.license && 'Unlicensed' !== pkg.license
  );

module.exports = findPackages;
