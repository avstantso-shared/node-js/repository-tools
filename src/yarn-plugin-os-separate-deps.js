const fs = require('fs');

function YarnPluginOsSeparateDeps({
  configFileName = './package.json',
  restoreRanges = true,
  warnings = { emptyConfig: false },
} = {}) {
  function readConfig() {
    const config = ((fs.existsSync(configFileName) &&
      JSON.parse(`${fs.readFileSync(configFileName)}`)) ||
      {})[YarnPluginOsSeparateDeps.plugin];

    if (!config && warnings?.emptyConfig)
      console.warn(
        `${YarnPluginOsSeparateDeps.plugin}: config is empty for path "${configFileName}"`
      );

    return config;
  }

  return {
    name: `plugin-${YarnPluginOsSeparateDeps.plugin}`,
    factory: (require) => {
      const config = readConfig();
      if (!config) return {};

      const changes = {};

      function reduceDependency(
        dependency,
        project,
        locator,
        initialDependency,
        { resolver, resolveOptions }
      ) {
        if (YarnPluginOsSeparateDeps.marker === dependency.range) {
          const info = config[dependency.name];
          if (!info)
            throw Error(
              `${YarnPluginOsSeparateDeps.name} "${dependency.name}" info not found`
            );

          const platformInfo = info[process.platform];
          if (!platformInfo)
            throw Error(
              `${YarnPluginOsSeparateDeps.name} "${dependency.name}" platformInfo not found for "${process.platform}"`
            );

          const newRange = info.common
            ? `${info.common}`.replace('${OS}', platformInfo)
            : platformInfo;

          const change = {
            oldRange: dependency.range,
            newRange,
          };

          changes[dependency.name] = change;

          dependency.range = newRange;
        }

        return dependency;
      }

      function afterAllInstalled(project, options) {
        [...project.storedPackages.values()].forEach((spkg) =>
          ['dependencies', 'devDependencies'].forEach((depKey) => {
            const deps = spkg[depKey];
            if (!deps) return;

            [...deps.values()].forEach((dep) => {
              const change = changes[dep.name];
              if (!change || dep.range !== change.newRange) return;

              dep.range = change.oldRange;
            });
          })
        );

        return project.persist();
      }

      return {
        hooks: {
          reduceDependency,
          ...(restoreRanges ? { afterAllInstalled } : {}),
        },
      };
    },
  };
}

YarnPluginOsSeparateDeps.plugin = 'os-separate-deps';
YarnPluginOsSeparateDeps.marker = `npm:${YarnPluginOsSeparateDeps.plugin}`;

module.exports = YarnPluginOsSeparateDeps;
