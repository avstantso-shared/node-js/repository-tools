const path = require('path');
const fs = require('fs');
const { spawnSync } = require('child_process');

const { gitlabFile } = require('./utils');

async function MonorepoByTemplate(
  monorepoUrl = '',
  currentPackage = {},
  usedPackages = {}
) {
  const [pkg, gitmodules] = await (async () => {
    const [rawPkg, rawGitmodules] = await Promise.all(
      [
        'package.json',
        '.gitmodules',
        // 'utils/yarn-plugin-workspaces-deps.js',
      ].map((file) => gitlabFile(monorepoUrl, file))
    );

    return [
      JSON.parse(rawPkg),
      rawGitmodules
        .split('[submodule "')
        .filter((m) => m)
        .map((m) =>
          m.split('\n').reduce((r, i) => {
            const [key, value] = i.split('=');
            if (key && value) {
              r[key.trim()] = value.trim();
            }

            return r;
          }, {})
        ),
    ];
  })();

  const [[curPkgSubScope, curPkgName]] = Object.entries(currentPackage);

  const allPkgSubScopes = [
    ...new Set([curPkgSubScope, ...Object.keys(usedPackages)]),
  ];

  const usedModules = Object.entries(usedPackages).reduce(
    (r, [subScope, names]) => {
      names.forEach((name) => {
        const m = gitmodules.find(
          ({ path }) => `packages/${subScope}/${name}` === path
        );
        m && r.push(m);
      });

      return r;
    },
    []
  );

  const currentRepoFiles = fs
    .readdirSync(process.cwd())
    .filter(
      (file) =>
        !file.startsWith(`ci-cd`) &&
        !['.git', '.gitignore', '.gitlab-ci.yml', 'packages'].includes(file)
    );

  const packagesPath = path.resolve(process.cwd(), 'packages');
  !fs.existsSync(packagesPath) && fs.mkdirSync(packagesPath);

  allPkgSubScopes
    .map((ss) => path.resolve(packagesPath, ss))
    .forEach((p) => !fs.existsSync(p) && fs.mkdirSync(p));

  const currentRepoPath = path.resolve(
    packagesPath,
    curPkgSubScope,
    curPkgName
  );
  !fs.existsSync(currentRepoPath) && fs.mkdirSync(currentRepoPath);
  currentRepoFiles.forEach((fileOrDir) =>
    fs.renameSync(
      path.resolve(process.cwd(), fileOrDir),
      path.resolve(currentRepoPath, fileOrDir)
    )
  );

  usedModules.forEach((m) => {
    const { stdout, stderr } = spawnSync('git clone', [m.url, m.path], {
      shell: true,
    });

    [stdout, stderr].forEach((o, i) => {
      o && (i ? console.error : console.log)(Buffer.from(o).toString('utf8'));
    });
  });

  fs.writeFileSync(
    path.resolve(process.cwd(), 'yarn-plugin-workspaces-deps.js'),
    [
      'const { YarnPluginWorkspacesDeps } = require("./packages/node/repository-tools");',
      'module.exports = YarnPluginWorkspacesDeps("./packages");',
    ].join('\r\n')
  );
  fs.writeFileSync(
    path.resolve(process.cwd(), '.yarnrc.yml'),
    [
      'compressionLevel: mixed',
      'enableGlobalCache: false',
      'plugins:',
      '  - ./yarn-plugin-workspaces-deps.js',
    ].join('\r\n')
  );

  //#region pkg
  delete pkg.description;
  delete pkg.devDependencies;
  delete pkg.dependencies;
  delete pkg.packageManager;

  pkg.name = `${pkg.name}--ci-cd--${curPkgSubScope}-${curPkgName}`;

  pkg.workspaces = allPkgSubScopes.map((ss) => `packages/${ss}/*`);

  Object.keys(pkg.scripts).forEach((s) => {
    const [subScope, pkgName] = s.split('--');

    if (subScope && pkgName) {
      const pkgNameArray = [pkgName, `${subScope}-${pkgName}`];

      if (
        (curPkgSubScope === subScope && pkgNameArray.includes(curPkgName)) ||
        (usedPackages[subScope] || []).some((usedPkg) =>
          pkgNameArray.includes(usedPkg)
        )
      )
        return;
    }

    delete pkg.scripts[s];
  });

  fs.writeFileSync(
    path.resolve(process.cwd(), 'package.json'),
    `${JSON.stringify(pkg, null, 2)}\r\n`
  );
  //#endregion
}

module.exports = MonorepoByTemplate;
