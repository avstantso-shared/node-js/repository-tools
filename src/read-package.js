const path = require('path');
const fs = require('fs');

function readPackage(packagePath, filter) {
  const file = path.resolve(packagePath, 'package.json');
  if (!fs.existsSync(file)) return;

  const pkg = JSON.parse(`${fs.readFileSync(file)}`);

  if (filter && !filter(pkg)) return;

  function save({ silent } = {}) {
    fs.writeFileSync(file, `${JSON.stringify(pkg, null, 2)}\r\n`);
    !silent && console.log(`\x1b[36m*\x1b[0m ${file}`);
  }

  return {
    get name() {
      return pkg.name;
    },
    get version() {
      return pkg.version;
    },
    pkg,
    file,
    save,
  };
}

module.exports = readPackage;
